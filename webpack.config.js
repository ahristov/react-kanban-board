// Add WebPack to use the included CommonsChunkPlugin
var webpack = require('webpack');
var path = require('path');

var config = {
	addVendor: function(name, path, parse) {
		this.resolve.alias[name] = path;
		if (!parse) {
			// this.module.noParse.push(new RegExp(path));
            this.module.noParse.push(new RegExp('^' + name + '$'));
		}
	},
    
    // We split the entry into two specific chunks. Our app and vendors. Vendors
    // specify that react should be part of that chunk
	entry: {
        app: ['webpack/hot/dev-server', './src/index.js'],
        vendors: ['mui.css']
    },
	resolve: { alias: {} },
    
    // We add a plugin called CommonsChunkPlugin that will take the vendors chunk
    // and create a vendors.js file. As you can see the first argument matches the key
    // of the entry, "vendors"
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
    ],
	output: {
        // If in production mode we put the files into the dist folder instead
        path: process.env.NODE_ENV === 'production' ? './dist' : './build',        
		filename: 'components.js'
	},
	module: {
		noParse: [],
        preLoaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'source-map'
            }
        ],    
		loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loaders: [
                    'react-hot',
                    'babel?presets[]=stage-0,presets[]=react,presets[]=es2015'
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'url?limit=8192',
                    'img'
                ]
            },            
			{ test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' }
		]
	}
};

// config.addVendor('react', m + '/react/react.min.js');
// config.addVendor('mui', bower_dir + '/mui/packages/cdn/js/mui.min.js');

var node_modules_dir = __dirname + '/node_modules';
var bower_components_dir = __dirname + '/bower_components';

config.addVendor('mui.css', node_modules_dir + '/muicss/lib/css/mui.min.css', true);

module.exports = config;

