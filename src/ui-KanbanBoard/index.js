import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';
import KanbanList from '../ui-KanbanList';

class KanbanBoard extends Component {
	render() {
		return (

			<div className="kanban-board">
				
				<KanbanList id="kanban-list-todo" title="To Do" cards={
					this.props.cards.filter((card) => card.status === "todo")
				} />

				<KanbanList id="kanban-list-in-progress" title="In Progress" cards={
					this.props.cards.filter((card) => card.status === "in-progress")
				} />

				<KanbanList id="kanban-list-done" title="Done" cards={
					this.props.cards.filter((card) => card.status === "done")
				} />

			</div>

		);
	}
} 


export default KanbanBoard;