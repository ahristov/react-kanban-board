import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';
import KanbanCheckList from '../ui-KanbanCheckList';

class KanbanCard extends Component {
	render() {
        return (
            <div className="kanban-card">
                <div className="card-title">{this.props.title}</div>
                <div className="card-details">
                    {this.props.description}
                    <KanbanCheckList    cardId={this.props.id}
                                        tasks={this.props.tasks} />
                </div>
            </div>
        );
	}
} 


export default KanbanCard;