import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';

class KanbanCheckList extends Component {
	render() {
        let tasks = this.props.tasks.map((task) => {
            return <li key={task.id} className="checklist-task">
                <input type="checkbox" defaultChecked={task.done} />
                {task.name}
                <a href="#" className="checklist-task-remove" />
            </li>
        });

        return (
            <div className="checklist">
                <ul>{tasks}</ul>
            </div>
        );
	}
} 

export default KanbanCheckList;