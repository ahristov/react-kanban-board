import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';
import KanbanCard from '../ui-KanbanCard';

class KanbanList extends Component {
	render() {
        var cards = this.props.cards.map((card) => {
            return <KanbanCard  id={card.id}
                                key={card.id}
                                title={card.title}
                                description={card.description}
                                tasks={card.tasks} />
        });
		
        return (
            <div className="kanban-list">
                <h1>{this.props.title}</h1>
                {cards}
            </div>
        );
	}
} 


export default KanbanList;