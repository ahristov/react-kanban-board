# React Kanban Board

## Description

This app contains the code I captured from following the [Pro React](http://www.pro-react.com/)
book from Cassio de Sousa Antonio.


I am using a clone of my [react-starter-kit](https://bitbucket.org/ahristov/react-starter-kit)
that provides some initial intrastructure and modular design.

This starter kit creates simple modular infrastructure for developing
React component apps and libraries and combines:

- webpack

- muicss

- stylus


## Usage

To use the code from this project:

1. Install the following packages globally

    npm install bower -g
    npm install webpack -g
    npm install webpack-dev-server -g
    npm install jsx-loader -g
    npm install -g babel-loader

1. Run from this directory

    npm install

1. Run webpack dev server


Start serve the project with hot reload run:

    webpack-dev-server --content-base build/

, or if you get javascript error:

    Uncaught Error: [HMR] Hot Module Replacement is disabled.

, then use:

    webpack-dev-server --hot --inline --content-base build/


Open the url: http://localhost:8080/



## Links

[Pro React](http://www.pro-react.com/)

[react-starter-kit](https://bitbucket.org/ahristov/react-starter-kit)

